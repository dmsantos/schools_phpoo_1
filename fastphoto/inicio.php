<?php 
session_start(); 
$logged = $_SESSION['logged'];

?>
<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <title>Login Sistema</title>
	
    <script src="assets/scripts/jquery.min.js"></script>		
	
	<!-- Latest compiled and minified CSS 
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	-->

	<!-- Optional theme -->
	<link rel="stylesheet" href="assets/styles/bootstrap.min.css" >
	

	<!-- Latest compiled and minified JavaScript Bootstrap -->
	<script src="assets/scripts/bootstrap.min.js"></script>	
	
	<!-- Latest compiled and minified JavaScript Social Bootstrap-->	
	<link rel="stylesheet" href="assets/styles/bootstrap-social.min.css" >		
	
	<!-- Font Awesome -->
	<link rel="stylesheet" href="assets/styles/font-awesome.min.css">
	
    <style>
        .center-component{
            float: none;
            margin: 0 auto;
        }
    </style>
</head>
<body style="background: #333">
    <br>
    <div class="container">	
<?php 

if ($logged == false){
?>
<div class="jumbotron" style="background: #FFF">
	Por favor, efetue login primeiro!<br/>
</div>
<META http-equiv="refresh" content="1;URL=http://moraisfotografia.com.br/dev/fastphoto/"> 
<?php
}else{		
	//var_dump($_SESSION);	
?>
	<div class="col-sm-6 col-md-6 col-lg-6 center-component">		
		<div class="row">
			<div class="jumbotron" style="background: #FFF">
		
			Usuario logado com sucesso!			
			<br/ >
			<div class="row">
				<div class="col-sm-4 col-md-4 col-lg-4 center-component">		

					<img src="<?php echo $_SESSION['user_photo']; ?>" height="80" width="80" />
					<br/>			
				</div>
			</div>
			<br/>
					
			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-12">		
				
					Usuario: <?php echo $_SESSION['user_fullname']; ?>
					<br/>
					Email: <?php echo $_SESSION['user_email']; ?>
					<br/>						
					
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-4 col-md-4 col-lg-4"></div>
				<div class="col-sm-4 col-md-4 col-lg-4"></div>
				<div class="col-sm-4 col-md-4 col-lg-4">
					<a href="http://moraisfotografia.com.br/dev/fastphoto/app/usuario/doLogout">Sair</a>
				</div>
			</div>
			
                </div>
            </div>
        </div>
    </div>	
	
	<div class="col-sm-12 col-md-12 col-lg-12 center-component">		
		<div class="row">
			<div class="jumbotron" style="background: #FFF">
				<pre>
<?php var_dump($_SESSION['user_info']); ?>
<br/>
Google Drive
<?php /*var_dump($_SESSION['user_drivefiles']); */?>
Google Calendar
<?php var_dump($_SESSION['user_calendar']); ?>

				</pre>
			</div>
		</div>
	</div>	
			
		
<?php } ?>				


	

</body>
</html>