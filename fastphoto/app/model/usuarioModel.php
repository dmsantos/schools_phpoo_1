<?php

require_once "dao/usuarioDAO.php";

Class usuarioModel{
	
	private $usuario;
	private $email;
	private $senha;
	
	public function __set($name, $value){
		
		if (property_exists($this, $name)){
			$this->$name = $value;
			return;
		}else{
			throw new Exception('Property n�o existe',1);
		}			
		
	}
	
	public function __get($name){
		
		return $this->$name;
		
	}
	
	
}