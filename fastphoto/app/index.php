<?php
//quebra o get recebido em array (explode)
$url = isset($_GET["url"]) ? explode("/", trim($_GET["url"])) : array();

//metodo default de controller
$metodo = "index";

//caso o primeiro segmento da url exista trata ele como o nome da classe do controler
if(isset($url[0]) && file_exists("controller/" . $url[0] . ".php")){
	$controller = $url[0];
	unset($url[0]);
}

//adiciona a classe ao include (require_once)
require_once "controller/" . $controller . ".php";

//instancia a classe
$controller = new $controller;

//caso o primeiro segmento da url exista trata ele como o metodo da classe do controler
if(isset($url[1]) && method_exists($controller, $url[1])){
	$metodo = $url[1];
	unset($url[1]);
}
//chama o metodo
$controller->$metodo();