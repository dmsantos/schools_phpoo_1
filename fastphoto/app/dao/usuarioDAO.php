<?php

require_once "core/database.php";
require_once "model/usuarioModel.php";

class usuarioDAO{
	
	private $pdo;
	
	public function __construct(){
		
		$this->pdo = Database::getInstance();		
		
	}
	
	public function doLogin(UsuarioModel $usuarioModel){
		
		try{
			
			$sql = 'SELECT * FROM usuarios WHERE email = :email AND senha = :senha LIMIT 0,1';
			
			$query = $this->pdo->prepare($sql);
			
			$email = $usuarioModel->__get('email');
			$senha = $usuarioModel->__get('senha');
			
			$query->bindParam('email', $email);
			$query->bindParam('senha', $senha);
			
			if ($query->execute() && $query->rowCount() == 1){

				session_start();
				$_SESSION['usuario'] = $query->fetch(PDO::FETCH_OBJ);
				$_SESSION['logged'] = true;
				return true;
				
			}else{
				return false;
			}				
			
			
		}catch (Exception $e){
			throw new Exception('Erro ao efetuar DAO '.$e->getMessage(),1);
		}
		
		
	}
	
	
}