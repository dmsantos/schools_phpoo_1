<?php

require_once "dao/usuarioDAO.php";
require_once "model/usuarioModel.php";

//GoogleLogin
require_once "libraries/Google/autoload.php";

class Usuario{
	
	private $usuarioDAO;
	private $usuarioModel;
		
	public function __construct(){
		
		$this->usuarioDAO = new usuarioDAO();
		
	}
	
	public function teste(){
		
		echo "controller workando";
		
	}
	
	
	public function doLogin(){
		
	    $this->usuarioModel = new usuarioModel();
		
	    $this->usuarioModel->__set("email", $_POST["usuario"]);
		$this->usuarioModel->__set("senha", $_POST["senha"]);
		
		if ($this->usuarioDAO->doLogin($this->usuarioModel)){
			echo "true";
			return;
		}else{
			echo "false";
			return;
		}		
		
	}
	
	public function doLogout(){
		
		session_start();		
		session_destroy();
		
		header("location:http://moraisfotografia.com.br/dev/fastphoto/");
		
	}
	
	public function doLoginGoogle(){
		
		$clientId = '534747576692-cn7g6ljjteeu23ll700in6i97lrdjedk.apps.googleusercontent.com';
		$clientSecret = 'hyRTWnu_Fo9jni9gVSNVUtSP';
		$redirectUri = "http://localhost/fastphoto/app/dashboard/loggedgoogle";		
		if ($_SERVER['SERVER_NAME'] == 'moraisfotografia.com.br'){
		$redirectUri = "http://moraisfotografia.com.br/dev/fastphoto/app/dashboard/loggedgoogle";
		}		
		
		$client = new Google_Client();
		
		$client->setClientId($clientId);
		$client->setClientSecret($clientSecret);
		$client->setRedirectUri($redirectUri);
		
		$client->addScope("email");
		$client->addScope("profile");
		$client->addScope("https://www.googleapis.com/auth/drive");
		$client->addScope("https://www.googleapis.com/auth/drive.readonly");
		$client->addScope("https://www.googleapis.com/auth/calendar.readonly");
		
		$authUrl = $client->createAuthUrl();
		
		header("location:$authUrl");
		
		
	}
	
	
}