<?php

require_once "dao/usuarioDAO.php";
require_once "model/usuarioModel.php";

//GoogleLogin
require_once "libraries/Google/autoload.php";

class Dashboard{
	
	private $usuarioDAO;
	private $usuarioModel;
		
	public function __construct(){
		
		$this->usuarioDAO = new usuarioDAO();
		
	}
	
	public function loggedgoogle(){
		
		if (isset($_GET['code'])){
			
			session_start();
			
			$clientId = '534747576692-cn7g6ljjteeu23ll700in6i97lrdjedk.apps.googleusercontent.com';
			$clientSecret = 'hyRTWnu_Fo9jni9gVSNVUtSP';
			$redirectUri = "http://localhost/fastphoto/app/dashboard/loggedgoogle";									
			if ($_SERVER['SERVER_NAME'] == 'moraisfotografia.com.br'){
			$redirectUri = "http://moraisfotografia.com.br/dev/fastphoto/app/dashboard/loggedgoogle";
			}		
			
			
			
			$client = new Google_Client();

			$client->setClientId($clientId);
			$client->setClientSecret($clientSecret);
			$client->setRedirectUri($redirectUri);			
			
			$serviceOAuth = new Google_Service_Oauth2($client);
			$serviceDrive = new Google_Service_Drive($client);	
			$serviceCalendar = new Google_Service_Calendar($client);
			
			$client->authenticate($_GET['code']);

			//UserInfo Perfil
			$user_info = $serviceOAuth->userinfo->get();
					
			//Google Drive
			$user_driveFiles = $serviceDrive->files->listFiles();
			
			//Google Calendar
			$user_calendar = $serviceCalendar->calendarList->listCalendarList();
			
			$_SESSION['access_token'] = $client->getAccessToken();
			$_SESSION['user_info'] = $user_info;
			$_SESSION['user_drivefiles'] = $user_driveFiles;
			$_SESSION['user_calendar'] = $user_calendar;
			$_SESSION['logged'] = true;
			
			//Info do Usuario
			$_SESSION['user_firstname'] = $user_info['givenName'];
			$_SESSION['user_lastname'] = $user_info['familyName'];
			$_SESSION['user_fullname'] = $user_info['name'];
			$_SESSION['user_email'] = $user_info['email'];
			$_SESSION['user_photo'] = $user_info['picture'];

						
			header('location:/dev/fastphoto/inicio.php');
			
		}else{
			echo "Error: No Google Auth Code";
		}
		
	}
	
	
	
}