<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <title>Login Sistema</title>
	
    <script src="assets/scripts/jquery.min.js"></script>		
	
	<!-- Latest compiled and minified CSS 
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	-->

	<!-- Optional theme -->
	<link rel="stylesheet" href="assets/styles/bootstrap.min.css" >
	

	<!-- Latest compiled and minified JavaScript Bootstrap -->
	<script src="assets/scripts/bootstrap.min.js"></script>	
	
	<!-- Latest compiled and minified JavaScript Social Bootstrap-->	
	<link rel="stylesheet" href="assets/styles/bootstrap-social.min.css" >		
	
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	
    <style>
        .center-component{
            float: none;
            margin: 0 auto;
        }
    </style>
</head>
<body style="background: #333">
    <br>
    <div class="container">

        <div class="col-sm-6 col-md-6 col-lg-6 center-component">		
			<div class="row">
				<div class="jumbotron" style="background: #FFF">
				
					<div class="row">

						<div class="col-sm-12 col-md-12 col-lg-12 center-component">
						Acesso
						</div>

					</div>			
					<br/>
				
					<form action="" id="formLogin">
						<div class="form-group">
							<label for="usuario">Usuario: </label>
							<input type="text" name="usuario" id="usuario" required class="form-control"/>
						</div>
						<div class="form-group">
							<label for="senha">Senha:</label>
							<input type="password" name="senha" id="senha" required class="form-control"/>
						</div>
						<div class="form-group">
							<input type="submit" class="btn btn-primary">

						</div>

						<div class="row">	
							<div class="col-sm-6 col-md-6 col-lg-6 center-component">
								<div class="form-group">						
									<a class="btn btn-block btn-social btn-facebook" onclick="">
										<span class="fa fa-facebook"></span> Login com Facebook
									</a>
									
									<a class="btn btn-block btn-social btn-google" href="app/usuario/doLoginGoogle">
										<span class="fa fa-google"></span> Login com Google+
									</a>						
								</div>
							</div>
						</div>

			</div>		
					
                </form>
                <div class="row text-center loading" style="display: none">
                    <div class="col-sm-6 col-md-6 col-lg-6 center-component">
                        <img src="http://img.ffffound.com/static-data/assets/6/f71fbabb835aebca4489ba2e0d5cd6aff3ad528c_m.gif" alt="" class="img-responsive">
                    </div>
                </div>
                <div class="row text-center erro alert alert-danger" style="display: none">

                </div>
            </div>
        </div>
    </div>
	
	<script>
	
		$(document).ready(function(){
			
			$("#formLogin").submit(function(event){
				
				$('.erro').hide();
				
				event.preventDefault();
				
				var usuario = $('#usuario').val();
				var senha = $('#senha').val();
				var retorno;
				var erro = false;
				
				$.ajax({
					url:"app/usuario/doLogin",
					data: {usuario:usuario, senha:senha},
					dataType: "HTML",
					type: "POST",
					async: true,
					beforeSend: function(){
						$(".loading").show();						
					},
					success: function(data){
						retorno = data;
					},
					error: function(){
						erro = true;
					},
					complete: function(){
						
						$(".loading").hide();
						
						if (erro){
							console.log("Erro no AJAX");
							mostrarMensagem("Erro na requisiçao AJAX");
							return;
						};
						
						console.log(retorno);
						if (retorno == "true"){
							console.log('OK');
							location.href = "inicio.php";
						}else{
							console.log('Problema');
							mostrarMensagem("Usuario/senha inválido");
							return;
						}
						
					}
					
				});				
			});			
		});
		
		var mostrarMensagem = function(mensagem){
			$('.erro').html(mensagem);
			$('.erro').show();
		};
	
	</script>


</body>
</html>